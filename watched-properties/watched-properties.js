const vm = new Vue({
  el: "#app",
  data: {
    prename: "Bob",
    surname: "Sinclair",
    fullname: "Bob Sinclair"
  },
  // If you need a data property that depends on
  // an other data property, you better use computed
  // property, than several watched properties
  watch: {
    prename: function(value) {
      this.fullname = value + " " + this.surname
    },
    surname: function(value) {
      this.fullname = this.prename + " " + value
    }
  },
  computed: {
    computedFullname() {
      return this.prename + " " + this.surname
    }
  }
})

vm.prename = "Paul"
vm.surname = "Simon"
