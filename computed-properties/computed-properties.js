const vm = new Vue({
  el: "#app",
  data: {
    message: "Hello Vue.js"
  },
  // Computed properties are reactive bindings to
  // data blocks. They feature caching to improve performance
  // for computational heavy data transformations
  // (eg. looping through a large array). If you do not need
  // caching -> use a method!
  computed: {
    reversedMessage: function() {
      return this.message.split("").reverse().join("")
    }
  },
  // Basically the same as computed properties, but without caching
  methods: {
    reverseMessageFn() {
      return this.message.split("").reverse().join("")
    }
  }
})

// Due to vue's reactivity, reversedMessage will always depend
// on the message data property.
console.log(vm.message);
vm.message = "Goodbye Vue.js"
console.log(vm.reversedMessage);
